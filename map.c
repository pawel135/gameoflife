#include <stdio.h>
#include "map.h"

int howManyAliveNeighbours(int y, int x){
  int aliveNeighbours = 0;
  int i,j;
 for(i = y-1; i <= y+1; i++){
    for(j = x-1; j <= x+1; j++){
      if(i!=y || j!=x)
        aliveNeighbours += point[i][j].prevState;
    }
  }

  return aliveNeighbours;

}

void changeGeneration(void){
  int row,col;
  extern int height, width;

  for(row = 1; row < height + 1 ; row++)
    for(col = 1; col < width + 1; col++)
      point[row][col].nextState = FSM[point[row][col].prevState].nextState[howManyAliveNeighbours(row,col)]; 
  
  // saving to prevState
  for(row = 1; row < height + 1; row++)
    for(col = 1; col < width + 1; col++)
      point[row][col].prevState = point[row][col].nextState;

}

void doManyGenerations(int n, char* outputPngFilename){
  int i;
  for (i = 0; i < n; i++){
#ifdef DEBUG
    printf("Numer generacji: %d\n", i);
#endif

    changeGeneration();  
#ifdef DEBUG
    printBinaryPicture();
#endif
    savePngFile(outputPngFilename, i+1);
  }
}


