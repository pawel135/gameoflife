#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include "input.h"
#include "pictures.h"
#include "map.h"

int main(int argc, char **argv){

  int col, row;
  int errorCount = 0;
 
  extern  int width, height;


  if (getInput(argc,argv) == EXIT_FAILURE){
    printf("Error: Incorrect input\n");
    errorCount++;
//    return EXIT_FAILURE;
  }


  readPngFile(inputPngFilename);



  makeBinaryPicture();

  for (row = 0; row < height+2 ; row++){
     for(col = 0; col < width+2; col++){
       if(point[row][col].prevState < 0 || point[row][col].prevState > 1 ){
          printf("Error: Inappriopriate state in [%d][%d] cell.\n",row,col);
          errorCount++;
       }



       if(row == 0 || row == height+1 || col == 0 || col == width+1)
         if(point[row][col].prevState != 0){
           printf("Error: States of phony boundary pixel lines are supposed to be equal to 0.\n");
           errorCount++;
         } 

     }
  }     
  
  for (row = 1; row < height+1 ; row++){
     for(col = 1; col < width+1; col++){
             
      if(howManyAliveNeighbours(row,col) < 0 || howManyAliveNeighbours(row,col) > 8){
        printf("Error: Wrong number of neighbours in [%d][%d] cell. \n", row,col);
        errorCount++; 
      }

       if(row == 1 || row == height || col == 1 || col == width)
        if(row == 1 && (col == 1 || col == width)  || row == height && (col == 1 || col == width))
          if(howManyAliveNeighbours(row,col) > 3){
            printf("Error: Wrong number of neighbours in [%d][%d] cell. \n", row,col);
            errorCount++;
          }
     }
  }


#ifdef DEBUG
  printf("Input file in binary format:\n");  
  printBinaryPicture();  
#endif

  doManyGenerations(numberOfGenerations,outputPngFilename);



  freePointStructure();
  if(errorCount == 0){
    printf("Test finished successfully with 0 errors.\n");
    return EXIT_SUCCESS;
  }
  else{
    printf("Test finished with %d errors.\n",errorCount);
    return EXIT_SUCCESS;
  }
}

