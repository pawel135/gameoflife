#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <time.h>
#include <png.h>
#include "pictures.h"
#include "map.h"

#define PNG_DEBUG 3
#define ALL_TO_ALIVE_RATIO 10 // value for initial map creation: 3 means 1 in 3 is alive

void abort_(const char * s, ...)
{
        va_list args;
        va_start(args, s);
        vfprintf(stderr, s, args);
        fprintf(stderr, "\n");
        va_end(args);
        abort();
}

int x, y;

int width, height;
png_byte color_type;
png_byte bit_depth;

png_structp png_ptr;
png_infop info_ptr;
int number_of_passes;
png_bytep * row_pointers;

void readPngFile(char* file_name){
         char header[8];    // 8 is the maximum size that can be checked

        /* open file and test for it being a png */
        FILE *fp = fopen(file_name, "rb");
        if (!fp)
                abort_("[read_png_file] File %s could not be opened for reading", file_name);
        fread(header, 1, 8, fp);
        if (png_sig_cmp(header, 0, 8))
                abort_("[read_png_file] File %s is not recognized as a PNG file", file_name);


        /* initialize stuff */
        png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

        if (!png_ptr)
                abort_("[read_png_file] png_create_read_struct failed");

        info_ptr = png_create_info_struct(png_ptr);
        if (!info_ptr)
                abort_("[read_png_file] png_create_info_struct failed");

        if (setjmp(png_jmpbuf(png_ptr)))
                abort_("[read_png_file] Error during init_io");

        png_init_io(png_ptr, fp);
        png_set_sig_bytes(png_ptr, 8);

        png_read_info(png_ptr, info_ptr);

        width = png_get_image_width(png_ptr, info_ptr);
        height = png_get_image_height(png_ptr, info_ptr);
        color_type = png_get_color_type(png_ptr, info_ptr);
        bit_depth = png_get_bit_depth(png_ptr, info_ptr);

        number_of_passes = png_set_interlace_handling(png_ptr);
        png_read_update_info(png_ptr, info_ptr);


        /* read file */
        if (setjmp(png_jmpbuf(png_ptr)))
                abort_("[read_png_file] Error during read_image");

        row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
        for (y=0; y<height; y++)
                row_pointers[y] = (png_byte*) malloc(png_get_rowbytes(png_ptr,info_ptr));

        png_read_image(png_ptr, row_pointers);

        fclose(fp);





        
}

void savePngFile(char* file_name, int offset){
  int y,x,value,file_name_length = 0;
  char* iterator;
  char offsetString[4];
  for (y=0; y< height; y++) {
    png_byte* row = row_pointers[y];
    for (x=0; x< width; x++) {
      png_byte* ptr = &(row[x*4]);
      value = ((point[y][x].prevState + 1) % 2) * 255; // translation 1->0*255 , 0->1*255
      ptr[0] = value;
      ptr[1] = value;
      ptr[2] = value;
      ptr[3] = 255;   // ptr[0] = 255; ptr[1] = 255; ptr[2] = 255; ptr[3] = 255;  // full white untransparent canvas

#ifdef DEBUG
      printf("savePngFile(), %d : Pixel at position [ %d - %d ] has RGBA values: %d - %d - %d - %d\n", offset, x, y, ptr[0], ptr[1], ptr[2], ptr[3]);
#endif

    }
}

    // editing output file name
      iterator = file_name; 
       while( *iterator++ != '\0' ){file_name_length++;}
       iterator = (char *) malloc ( file_name_length * sizeof(char) + 4 * sizeof(char));
       memmove( iterator, (const void*) file_name, file_name_length * sizeof(char));
       file_name = iterator; 
       while( *iterator != '.' ){
         iterator++;
       }
       *iterator = '\0';
       sprintf(offsetString ,"%d", offset);
       file_name = strcat(strcat( file_name, offsetString), ".png");
#ifdef DEBUG
       printf("Printing to file named: %s\n",file_name); 
#endif
           
       // create file 
        FILE *fp = fopen(file_name, "wb");
        if (!fp)
                abort_("[write_png_file] File %s could not be opened for writing", file_name);


        // initialize stuff 
        png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

        if (!png_ptr)
                abort_("[write_png_file] png_create_write_struct failed");

        info_ptr = png_create_info_struct(png_ptr);
        if (!info_ptr)
                abort_("[write_png_file] png_create_info_struct failed");

        if (setjmp(png_jmpbuf(png_ptr)))
                abort_("[write_png_file] Error during init_io");

        png_init_io(png_ptr, fp);


        // write header
        if (setjmp(png_jmpbuf(png_ptr)))
                abort_("[write_png_file] Error during writing header");

        png_set_IHDR(png_ptr, info_ptr, width, height,
                     bit_depth, color_type, PNG_INTERLACE_NONE,
                     PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

        png_write_info(png_ptr, info_ptr);


        // write bytes
        if (setjmp(png_jmpbuf(png_ptr)))
                abort_("[write_png_file] Error during writing bytes");

 
        png_write_image(png_ptr, row_pointers);


        // end write
        if (setjmp(png_jmpbuf(png_ptr)))
                abort_("[write_png_file] Error during end of write");

        png_write_end(png_ptr, NULL);

        fclose(fp);
  
}

void makeBinaryPicture(void){
  point = malloc ( (height+2) * sizeof *point);
  int y,x,i;
  for (y=0; y < height+2; y++) {
    *(point + y) = malloc ( (width+2) * sizeof **point);
  }

  for (y=0; y < height; y++) {
    png_byte* row = row_pointers[y];
    for (x=0; x < width; x++) {
      png_byte* ptr = &(row[x*4]);

#ifdef DEBUG
      printf("makeBInaryPicture(): Pixel at position [ %d - %d ] has RGBA values: %d - %d - %d - %d\n", x, y, ptr[0], ptr[1], ptr[2], ptr[3]);
#endif

      point[y+1][x+1].x = x;
      point[y+1][x+1].y = y;
      point[y+1][x+1].prevState = ((ptr[0]/220 + ptr[1]/220 + ptr[2]/220)/3 + 1 ) % 2; // if each from (R,G,B) value is >= 220 (almost white), then cell is dead -> value in structure is 0 
                     
      // ptr[0] = 255; ptr[1] = 255; ptr[2] = 255; ptr[3] = 255;  // full white untransparent canvas
     }
  }
  


 for(y = 0; y < height + 2; y++){
      point[y][0].y = y; 
      point[y][0].x = 0; 
      point[y][0].prevState = 0; 

      point[y][width+1].x = width+1;
      point[y][width+1].y = y;
      point[y][width+1].prevState = 0;
  }
  for(x = 0; x < width + 2; x++){

      point[0][x].x = x; 
      point[0][x].y = 0; 
      point[0][x].prevState = 0; 
  
      point[height+1][x].x = x; 
      point[height+1][x].y = height+1; 
      point[height+1][x].prevState = 0; 
  }

  
}

void printBinaryPicture(void){
  int row,col;
  for(row = 1; row < height+1; row++){
    for(col = 1; col < width+1; col++){
      printf("%d",point[row][col].prevState);
    }
    printf("\n");
  }
  printf("\n\n");
}

void freePointStructure (void){
      
    int row,col;
    for (row = 0; row < height+2 ; row++){
      free(*(point+row));
    }
    free(point);
    
     // cleanup heap allocation
     for (y=0; y<height; y++)
           free(row_pointers[y]);
     free(row_pointers);


}
