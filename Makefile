GCC=gcc -Wall -ansi --pedantic
PNG_INPUT_NAME1=picture
PNG_OUTPUT_NAME1=pictureResult
PNG_INPUT_NAME2=pictureSmall
PNG_OUTPUT_NAME2=pictureSmallResult
	
input.o: input.c input.h
	gcc -c input.c
main.o: main.c input.h pictures.h map.h
	gcc -c main.c
map.o: map.c map.h
	gcc -c map.c
pictures.o: pictures.c pictures.h map.h
	gcc -c pictures.c


all: main.c input.c input.h pictures.c pictures.h map.c map.h
	$(GCC) main.c input.c pictures.c map.c -lpng -lm -lz

al: main.c input.c input.h pictures.c pictures.h map.c map.h
	gcc  main.c input.c pictures.c map.c -lpng -lm -lz

alld: main.c input.c input.h pictures.c pictures.h map.c map.h
	$(GCC) main.c input.c pictures.c map.c -DDEBUG -lpng -lm -lz

ald: main.c input.c input.h pictures.c pictures.h map.c map.h
	gcc main.c input.c pictures.c map.c -DDEBUG -lpng -lm -lz

alasd: main.c input.c input.h pictures.c pictures.h map.c map.h
	gcc -c main.c input.c pictures.c map.c -DDEBUG -lpng -lm -lz
	objdump -d main.o input.o pictures.o map.o

run: a.out 
	./a.out -i $(PNG_INPUT_NAME1).png -o $(PNG_OUTPUT_NAME1).png -n 1000

runsm: a.out 
	./a.out -i $(PNG_INPUT_NAME2).png -o $(PNG_OUTPUT_NAME2).png


example: shortExample.c
	gcc shortExample.c -lpng -lm -lz

exrun: a.out
	./a.out $(PNG_INPUT_NAME1).png $(PNG_INPUT_NAME1).png


exsmrun: a.out
	./a.out $(PNG_INPUT_NAME2).png $(PNG_INPUT_NAME2).png

test: test.c input.c input.h pictures.c pictures.h map.c map.h 
	gcc test.c input.c pictures.c map.c -DDEBUG -lpng -lm -lz

testrun: a.out
	./a.out -i $(PNG_INPUT_NAME2).png -o $(PNG_OUTPUT_NAME2).png -n 10

inputtest: a.out
	-./a.out
	-./a.out -i input
	-./a.out -i $(PNG_INPUT_NAME2).png 
	-./a.out -i $(PNG_INPUT_NAME2).png -n 12
	-./a.out -i $(PNG_INPUT_NAME2).png -o $(PNG_OUTPUT_NAME2).png -n -10  
	-./a.out -i $(PNG_INPUT_NAME2).png -o $(PNG_OUTPUT_NAME2).png -n 10  
	-./a.out -i $(PNG_INPUT_NAME2).png -o $(PNG_OUTPUT_NAME2).png

clean:
	-rm *.o *.out
	-rm $(PNG_OUTPUT_NAME1)*.png $(PNG_OUTPUT_NAME2)*.png



