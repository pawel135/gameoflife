#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include "input.h"
#include "pictures.h"
#include "map.h"

int main(int argc, char **argv){

  if (getInput(argc,argv) == EXIT_FAILURE)
    return EXIT_FAILURE;
 
  readPngFile(inputPngFilename);  
  makeBinaryPicture();
  
#ifdef DEBUG
  printf("Input file in binary format:\n");  
  printBinaryPicture();  
#endif

  doManyGenerations(numberOfGenerations,outputPngFilename);

  freePointStructure();
  return EXIT_SUCCESS;

}

